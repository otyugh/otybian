#/bin/sh
set -xeu

#Need to be root
[ $(id -u) -ne 0 ] && { echo "I only obey to root, churl"; exit 1; }

#Need live-build package
[ ! -x '/usr/bin/lb' ] && { echo "This script needs live-build !"; exit 1; }

#Be in the right context (if called from elsewhere)
cd "$(cd "$(dirname "$0")" && pwd -P)"

function curl2() {
	[ -d "$cachedir" ] || mkdir "$cachedir"
	f="$cachedir/$(basename $2)"
	#wasn't dled or older than 7day : redl
	if ! [ -f "$f" ] || [ $(find "$f" -mtime +7) ]
	then
		curl -Lq "$1" -s -o "$f"
	fi
	cp "$cachedir/$(basename $2)" "$2"
}

function append() {
	if [ ${1: -1} == '/' ]
	then
		mkdir -p "$2"
		[ -d "$build/$1" ] && rsync -a "$build/$1" "$2"
	else
		mkdir -p "$(dirname "$2")"
		[ -f "$build/$1" ] && { cat "$build/$1" >> "$2"; echo '' >> "$2"; }
	fi
}

#Config path
recipedir="./recipes"
var="$recipedir/var.sh"; source $var
livedir="./live-build_$FLAVOUR"
cachedir="./cache"

log="cook.log"

config="config.sh"
configP="$livedir/auto/config"
pkglist="pkglist.txt"
pkglistP="$livedir/config/package-lists/autobuild.list.chroot"
hook="hook.sh"
hookP="$livedir/config/hooks/normal/9999-autobuild.hook.chroot"
bootloader="bootloader/"
bootloaderP="$livedir/config/includes.binary"

chroot="chroot/"
chrootP="$livedir/config/includes.chroot"
adduser="adduser.sh"
adduserP="$chrootP/usr/local/sbin/adduser.local"
lateseed="lateseed.sh"
lateseedP="$chrootP/usr/share/tmp/lateseed.sh"
lateseedPR="/usr/share/tmp/lateseed.sh" #relative path

script="script.sh"
scriptP="$livedir/script.sh"

#Remove old setup
rm -rf "$configP" "$pkglistP" "$hookP" "$adduserP" "$bootloaderP" "$chrootP"

#PRE-setup
mkdir -p "$bootloaderP" "$chrootP"

mkdir -p "$(dirname "$configP")" "$(dirname "$pkglistP")" "$(dirname "$hookP")" "$(dirname "$adduserP")" "$(dirname "$lateseedP")"
touch "$configP" "$pkglistP" "$hookP" "$adduserP" 

echo -e '#!/bin/sh
lb config noauto \\' > "$configP"

echo -e "#!/bin/bash
set -xeu
mkdir -p '/etc/dconf/profile/'
echo 'user-db:user
system-db:site' > '/etc/dconf/profile/user'
mkdir -p '/etc/dconf/db/site.d/'
" > "$hookP"

echo -e '#!/bin/bash
set -xeu
exec >> "/var/log/live-build.log.lateseed" 2>&1' > "$lateseedP"

echo -e '#!/bin/bash
set -xeu' > "$scriptP"

echo -e '#!/bin/bash
set -xeu
exec >> "/var/log/live-build.log.adduser" 2>&1
echo "#adduser"

#Load userdirs
USER="$1"
HOME="$4"; source "$HOME/.config/user-dirs.dirs"
uid=$2
gid=$3
export PATH="$PATH:/usr/local/bin"' > "$adduserP"


build="./"
append "$var" "$hookP"
append "$var" "$adduserP"
append "$var" "$lateseedP"

find "$recipedir" -mindepth 2 -maxdepth 2 -type d | sort | while read build
do
	#If there is an extension, it should be the same flavour
	if [[ "$(basename $build)" == *.$FLAVOUR ]] || [[ "$(basename $build)" != *.* ]]
	then
		append "$chroot" "$chrootP"
		append "$bootloader" "$bootloaderP"

		append "$pkglist" "$pkglistP"

		echo "set -xeu" >> "$hookP"
		append "$hook" "$hookP"
		append "$adduser" "$adduserP"
		append "$lateseed" "$lateseedP"

		[ -e "$build/$script" ] && echo -e "recette=$build" >> "$scriptP"
		append "$script" "$scriptP"
		#CHRONOTASK
		echo 'echo -e "\nADDUSER:$SECONDS'":$build"'\n"' >> "$adduserP"
		echo 'echo -e "\nPRESCRIPT:$SECONDS'":$build"'\n"' >> "$scriptP"
		echo 'echo -e "\nHOOK:$SECONDS'":$build"'\n"' >> "$hookP"
		echo 'echo -e "\nLATESEED:$SECONDS'":$build"'\n"' >> "$lateseedP"
	fi
done || true

#Prebuild
source "$scriptP"


#POST-setup
find "$bootloaderP/seed" -type f -print0 | while read -d '' seed
do
	echo -e "\nd-i preseed/late_command string in-target /bin/bash $lateseedPR" >> $seed
done
echo '    "${@}"' >> "$configP"

#echo '[ $(df "/" --output=source | tail -n1) != "overlay" ] && dconf update' >> "$adduserP"
echo -e '\ndconf update' >> "$hookP"

echo '	update-grub
	update-initramfs -u' >> "$lateseedP"
chmod +x "$configP"
chmod +x "$adduserP"

#Build
cd "$livedir"
lb clean
lb config
lb build | tee "$log"
echo "Temps passé : $(($SECONDS/60)) minutes"
sleep 1
