# Construction

## Copier le constructeur d'ISO chez-vous
`git clone "https://framagit.org/otyugh/otybian"`

## Construire l'ISO tel quelle
`./cook.sh`

# Screenshot
![grub](screenshot/1-grub-legacy.png)
![installateur](screenshot/2-installer.png)
![Bureau](screenshot/3-desktop.png)
![Menu whisker](screenshot/3-menu.png)
![Menu xfce classique](screenshot/3-menu-alternate.png)
![Gestionnaire de fichier](screenshot/4-thunar.png)
![Firefox et marques pages](screenshot/5-firefox.png)
![Accéder à des site en .onion avec privoxy](screenshot/6-ffonion.png)

# Applications préinstallées
## Graphisme : tout ce qui est image
* Gimp : édition d'image
* Shotwell : gestion d'image et retouche simple (recadrage...)
* Simple-scan : numérisation
* Gpicview : visualisation, derushage...

## Multimedia : tout ce qui est audio/video
* Asunder : rip CD
* Handebreak : rip DVD/Blueray
* Audacity : édition son
* Cheese : photo webcam
* Clementine : gestion musique/podcast
* Smplayer : lecteur multimédia

## Bureautique : tout ce qui est texte
* Libreoffice calc : tableur "excel ©"
* Libreoffice impress : présentation "powerpoint ©"
* Libreoffice writer : éditeur de document "word ©"
* Mousepad : bloc note

## System : tout ce qui demande d'être administrateur
* Gparted : gestion de partition
* GSmartControl : gestion de l'état SMART des supports
* Logiciel : installation/désinstallation de logiciel
* Lxtask : gestionnaire de tâche
* Terminal-xfce4 : terminal
* Utilisateur et groupes : gerer les utilisateurs
* Xkill : tuer une fenêtre

## Internet : tout ce qui est en ligne
* Firefox : client web
* QuiteRss : lecteur de flux d'actualité
* Thunderbird : client mail
* Deluge : client bitorrent
* Hexchat : client irc

## Accessoire : tout ce qui n'entre pas ailleurs
* Galculator : calculatrice
* Grsync : outil de backup
* Keepassxc : coffre fort à mot de passe
* Pavucontrol : gestionnaire de son
* Thunar : gestionnaire de fichier

## Script : mes ajouts bricolo
* Youtube-dl-gui (script) : télécharger facilement video/audio à partir d'une adresse web
* Mise à jour : script de mise à jour en un click
* Aide à l'installation des firmwares privateurs

