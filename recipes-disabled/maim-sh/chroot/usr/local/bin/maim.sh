#!/bin/sh
appname="$(basename $0)"
shotname="$(date +%s).png"

out=$(zenity --list --text "Prise de capture d'écran" --title "$appname" \
--column "id" --column "action" --hide-header --hide-column 1 \
"0" "Capturer l'écran" "1" "Capturer une fenêtre" "2" "Capturer une zone") || exit 0

. $HOME/.config/user-dirs.dirs

case $out in
0) maim "$XDG_DESKTOP_DIR/$shotname"
;;
1) maim -st 9999999 "$XDG_DESKTOP_DIR/$shotname"
;;
2) maim -s "$XDG_DESKTOP_DIR/$shotname"
;;
esac
