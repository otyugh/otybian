echo "* Kazam's config"
f="$HOME/.config/kazam/kazam.conf"
if [ -f "$f" ]; then
	sed -ri "s|^(autosave_picture_dir = ).*|\1$XDG_DESKTOP_DIR|" "$f"
	sed -ri "s|^(autosave_video_dir = ).*|\1$XDG_DESKTOP_DIR|" "$f"
fi