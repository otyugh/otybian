## Initialisation des mirroirs

Garde en mémoire un synchronisation avec les miroirs debian (apt update) lors de la construction de l'image : permet d'avoir gnome-software/apt search fonctionnel dès le premier démarrage, même sans accès internet lors de l'installation
