## Lanceurs modifiés

Pour avoir des nom et des descriptions d'applications plus homogènes et intuitives, beaucoup d'entrées ont été renommées. 

Il a aussi été décidé de masquer certaines applications qui n'ont d'usage que pour visionner un contenu passivement (lecteur de PDF, d'ebook, d'images).

La catégories "Paramètres" a aussi été masqué, et mais tout son contenu est bien-sûr accessible !
