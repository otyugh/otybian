if [ $uid -eq 1000 ]
then
	echo "* [Once] Auto-update .desktop after apt install && apt remove"
	echo 'DPkg::Post-Invoke {"custom-desktop || true";};' >> '/etc/apt/apt.conf.d/99hack'
fi

echo "* Remove unused .desktop"
bash 'custom-desktop' 'remove'