conf="$chrootP/etc/skel/.config/Thunar/uca.xml"
sed -i '$ d' "$conf"
echo "<action>
        <icon>catfish</icon>
        <name>Rechercher ici</name>
        <unique-id>1560302039301448-1</unique-id>
        <command>catfish %f</command>
        <description>Recherche de fichier par nom</description>
        <patterns>*</patterns>
        <directories/>
</action>
</actions>" >> "$conf"