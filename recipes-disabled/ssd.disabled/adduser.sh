if [ $uid -eq 1000 ]
then
	#find the "/" mounted drive
	dev=$(grep -m1 -oE '[^ #]+ +/ +' '/etc/fstab' | cut -d' ' -f1 | cut -d'=' -f2)
	#check if it's livesession or ssd/hdd
	[ $dev != "overlay" ] && isSsd=$(lsblk -no ROTA "/dev/disk/by-uuid/$dev") || isSsd=1
	#move /tmp to RAM
	#[ $isSsd -eq 0 ] && echo "tmpfs /tmp tmpfs noatime,nosuid 0 0" >> "/etc/fstab"

	#find the swap
	dev=$(grep -m1 -oE '[^ #]+ +[^ ]+ +swap' '/etc/fstab' | cut -d' ' -f1 | cut -d'=' -f2)
	if [ ! -z "$dev" ] && [ $dev -eq 0 ]
	then
		echo "* Swap waaaaaay less than it would on a mechanical drive(not totally)"
		echo "vm.swappiness=1" >> '/etc/sysctl.conf'
	fi
fi