#Thunar pdify
conf="$chrootP/etc/skel/.config/Thunar/uca.xml"
sed -i '$ d' "$conf"
echo "<action>
	<icon>pdfshuffler</icon>
	<name>Convertir en PDF</name>
	<unique-id>1560386496023532-3</unique-id>
	<command>zerr pandoc --pdf-engine=xelatex -f markdown -t latex -s %f -o %f.pdf -V lang=fr --template default.latex</command>
	<description>Markdown vers PDF</description>
	<patterns>*.md</patterns>
	<text-files/>
</action>
</actions>" >> "$conf"
