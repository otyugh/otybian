---
title: Mon exemple de titre
author: John Smith
date: 01/12/2020
logo: /usr/share/icons/gnome/256x256/places/debian-swirl.png
abstract: Un résumé contenant tout ce qu'on peut faire en markdown !
---

# Les bases

## Paragraphes
Premier paragraphe.

Second paragraphe.

Troisième
 paragraphe.


## Styles
On a *l'italique*, le **gras** ! 

Le soulignage n'existe pas (considéré comme une relique de l'époque des machines à écrire).


## Listes
### Liste simple
* Premier élément
* Deuxième élément
	* Premier sous-élément
	* Second sous-élément
	* Troisième sous-élément
* Troisième élément

### Liste numérotée
1. Premier élément
1. Deuxième élément
	1. Premier sous-élément
	1. Second sous-élément
	1. Troisième sous-élément
1. Troisième élément


## Citations
Ça c'est un texte normal.

> Ça c'est une citation
>
> Et ça c'est un nouveau paragraphe

Retour au texte normal.

## Images
Bien sûr on peut ajouter des images

![Un exemple d'image](/usr/share/icons/gnome/48x48/emotes/face-angel.png)

# Conclusion
On peut faire un joli document avec pas grand chose !

Pour ceux qui veulent des choses compliquées, c'est possible.



# Possibilités avancées
## Séparateur
***

## Notes de bas de page
J'arrête pas de me faire frag[^frag] par des HLs[^hl], si c'est comme ça j'AFK[^afk] !

[^frag]: Se faire tuer
[^hl]: Hight Level : de haut niveau
[^afk]: Away From Keyboard : littéralement "s'écarter du clavier" ; s'arrêter de jouer

## Tableau
Taille  Matière      Couleur
----  ------------  ------------
9     cuir          marron
10    chanvre       naturel
11    verre         transparent

Table: Tableau de text

## Formules mathématiques
$$I = \int \rho R^{2} dV$$

## Pour les codeurs
On a un style de texte spécial pour le code :

~~~
define foobar() {
    print "Écrire du code";
}
~~~

Qu'on peut aussi appliquer pour `un seul mot`.
