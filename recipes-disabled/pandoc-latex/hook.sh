t="/etc/skel/Bureau/${ISONAME:-Debian customisée}.md"
if [ -f "$t" ] && hash "pandoc"
then
	pandoc -f markdown -t latex --pdf-engine=xelatex -s "$t" -o "${t%.*}.pdf" -s -V lang=fr --template default.latex
	rm "$t"
fi