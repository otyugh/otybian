#Copier le MDP utilisateur en MDP root
userpw=$(grep -Ev '^[^:]+:(\*|\!\!):' '/etc/shadow' | tail -n1 | cut -d':' -f2)
sed -ri "s|^(root:)[^:]*(:.*)|\1$userpw\2|" '/etc/shadow'
