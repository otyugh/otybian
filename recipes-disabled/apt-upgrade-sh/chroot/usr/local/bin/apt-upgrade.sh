#!/bin/bash
#set -x
z="--width=450 --height=0 --title=$(basename $0)"
errFile="$(mktemp)"
buffer="$(mktemp)"
prebuffer="$(mktemp)"
trap "rm -f "$errFile" "$buffer" "$prebuffer"" EXIT


#Pré-question
#upgrades=$(apt list --upgradeable 2>/dev/null | awk '/\// {printf("\t- %s %s\n", $1,$2)}')
upgrades=$(apt-get dist-upgrade -s | grep -E -m1 "^[0-9]+ mis à jour")
upgradesC=$(echo "$upgrades" | sed -r 's|([0-9]+)[^0-9]+([0-9]+).*|\1\2|')


if [ "$upgradesC" -le 0 ] #0 MàJ
then
	zenity $z --info --text="Vous êtes déjà à jour\n<sub>$upgrades</sub>"
	exit 0
else #MàJ dispo
	if ! [ "$1" == "skip" ]
	then
		zenity $z --question --text="<b>Mettre à jour maintenant ?</b>\n<sub>$upgrades</sub>"
		[ $? -ne 0 ] && exit 0
	fi
	if [ $(id -u) -ne 0 ] #Pas root
	then
		pkexec "$0" "skip"
		exit 0
	fi
fi


function zenError() {
	wait -n $job $zpid
	ret=$?
	if ! kill -s 0 $zpid 2>'/dev/null' #zenity a été fermé
	then
		kill -s 15 $job
		zenity $z --warning --text="<b>Vous avez interrompu le processus de mise à jour</b> ; relancez-le plus tard !"
		exit 1
	elif [ $ret -ne 0 ]
	then
		echo "100"
		zenity $z --error --text="<b>Erreur à $stage :</b>\n$(cat "$errFile")"
		exit 1
	fi
}

#Start zenity
tail -f --pid=$$ "$buffer" | zenity $z --progress --auto-close --text='Initialisation...' &
zpid=$!

#Verif
#0%
stage="0/3 Verification des paquets : dpkg --configure -a"
echo "#$stage" >> "$buffer"
dpkg --configure -a --force-all 2>"$errFile" &
job=$!; zenError

stage="1/3 Verification des paquets : apt install -f"
echo "#$stage" >> "$buffer"
apt-get -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" --yes --fix-broken install 2>"$errFile" 1>'/dev/null' &
job=$!; zenError

#dl => 50%
stage="2/3 Téléchargement des MàJ : initialisation"
echo "#$stage" >> "$buffer"
sleep 1
apt-get -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" --yes dist-upgrade --download-only -o APT::Status-Fd=1 2>"$errFile" 1>"$prebuffer" &
job=$!
tail -f --pid=$job "$prebuffer"  | awk -W interactive -F ':' '/^dlstatus/ {printf("#2/3 Téléchargement des MàJ : %.2f%%\n%d\n", $3,$3/2)}' 1>>"$buffer" &
zenError

#%50 + maj =>100
stage="3/3 Application des MàJ"
echo "#$stage" >> "$buffer"
sleep 1
apt-get -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" --yes dist-upgrade -o APT::Status-Fd=1 2>"$errFile" 1>"$prebuffer" &
job=$!
tail -f --pid=$job "$prebuffer"  | awk -W interactive -F ':' '/^pmstatus/ {printf("#Installation des MàJ : %.2f%%\n%d\n", $3,$3/2+50)}' 1>>"$buffer" &
zenError

zenity $z --info --text="Mise à jour effectuée !"
