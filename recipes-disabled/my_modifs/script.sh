t="$chrootP/etc/skel/Bureau/${ISONAME:-Debian customisée}.md"

echo "---
title: Debian ${FLAVOUR} (${ISONAME})
author: $PUBLISHER
logo: /usr/share/icons/gnome/256x256/places/otybian.png
abstract: Ce système basé 100% sur Debian avec l'environnement Xfce. Il ne contient que du logiciel libre. Le git du projet se trouve [ici](https://framagit.org/otyugh/otybian) !
---

L'objectif de ce projet est d'avoir un système léger, complet, et facile d'accès - comme Ubuntu - mais en essayant de faire mieux.

__Configurations__

- Pré-réglage de la plupart des applications pour une prise ne main facilité
- Firefox avec des plugins vitaux (ublock origin + httpseverywhere)
- Lire/transcoder des DVDs/CDs (avec libdvdcss)
- Utilisation des DNS de FAIs engagés (FDN et LDN)
- Les applications qui n'ont pas *intêret* à être lancé plusieur fois ne le seront plus
- La description des logiciels a été rendu plus homogène

__Fonctionalités__

- Convertir des média en mp3 en 2 clics (avec ffmpeg)
- Convertir un text markdown en PDF en 2 clics (avec pandoc)
- Télécharger des vidéos de n'importe quelle plateforme web en 3 clics (avec youtube-dl)
- Mettre à jour simplement/rapidement et gérer ses pilotes privateur facilement (avec apt-get)

"