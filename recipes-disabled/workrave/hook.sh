echo "	[org/workrave/timers/daily-limit]
	monitor=''
	snooze=1200
	activity-sensitive=true
	limit=14400

	[org/workrave/timers/micro-pause]
	auto-reset=30
	snooze=150
	activity-sensitive=true
	limit=180

	[org/workrave/timers/rest-break]
	auto-reset=600
	snooze=180
	activity-sensitive=true
	limit=2700

	[org/workrave/distribution]
	port=27273
	reconnect-attempts=3
	reconnect-interval=15

	[org/workrave/gui/main-window]
	cycle-time=10
	head=0
	y=300
	x=300
	always-on-top=false
	enabled=false

	[org/workrave/gui]
	trayicon-enabled=true

	[org/workrave/gui/applet]
	cycle-time=10
	enabled=false

	[org/workrave/gui/breaks/rest-break]
	enable-shutdown=true
	ignorable-break=true
	auto-natural=true
	skippable-break=true

	[org/workrave/gui/breaks]
	block-mode=0

	[org/workrave/gui/breaks/daily-limit]
	skippable-break=true
	ignorable-break=true

	[org/workrave/gui/breaks/micro-pause]
	skippable-break=true
	ignorable-break=true

	[org/workrave/breaks/daily-limit]
	enabled=true

	[org/workrave/breaks/micro-pause]
	enabled=false

	[org/workrave/breaks/rest-break]
	enabled=true

	[org/workrave/sound]
	volume=30

	[org/workrave/sound/events]
	micro-break-started='/usr/share/sounds/workrave/subtle/micro-break-started.wav'
	exercises-ended='/usr/share/sounds/workrave/subtle/exercises-ended.wav'
	break-prelude='/usr/share/sounds/workrave/subtle/break-prelude.wav'
	rest-break-started='/usr/share/sounds/workrave/subtle/rest-break-started.wav'
	daily-limit='/usr/share/sounds/workrave/subtle/daily-limit.wav'
	micro-break-ended='/usr/share/sounds/workrave/subtle/micro-break-ended.wav'
	exercise-step='/usr/share/sounds/workrave/subtle/exercise-step.wav'
	rest-break-ended='/usr/share/sounds/workrave/subtle/rest-break-ended.wav'
	exercise-ended='/usr/share/sounds/workrave/subtle/exercise-ended.wav'
	break-ignored='/usr/share/sounds/workrave/subtle/break-ignored.wav'
" >> '/etc/dconf/db/site.d/workrave'
