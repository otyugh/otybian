#!/bin/bash
set -xu

#Vars
title="vnc-gui"
width="600"
height="200"
zenity="--title $title --width $width --height $height"

cert_validity=$((365*30)) #30y
maindir="$HOME/.config/$title"

srv_vncviewer_port=5910
cli_x11vnc_port=5920

cli="$maindir/cli"
	cli_stunnel_cnf="$cli/stunnel.cnf"

	clipid="$cli/pid"
		cli_stunnel_pid="$clipid/stunnel.pid"
		#cli_x11vnc_pid="$clipid/x11vnc.pid"
	cliprofile="$cli/profile"

mkdir -p "$cli" "$clipid" "$cliprofile"


srv="$maindir/srv"
	srv_stunnel_cnf="$srv/stunnel.cnf"
		lastport="$srv/lastport"
		key="$srv/privkey.pem"
		cert="$srv/cert.pem"
		confssl="$srv/openssl.cnf"

	srvpid="$srv/pid" 
		srv_stunnel_pid="$srvpid/stunnel.pid"
		#srv_vncviewer_pid="$srvpid/vncviewer.pid"

mkdir -p "$srv" "$srvpid"

add="Ajouter un dépanneur"
del="Supprimer un dépanneur"

#INIT
[ ! -f "$confssl" ] && echo "[req]
distinguished_name = req_distinguished_name
req_extensions = v3_req
prompt = no
default_bits = 2048

[req_distinguished_name]
C = FR
ST = .
L = .
O = .
OU = .
CN = .

[v3_req]
" >> "$confssl"

#Functions
zenity2array() {
	readarray -td'|' z <<< "$z|" #UGLY WORKAROUND 1/2
	unset z[$((${#z[@]}-1))] #UGLY WORKAROUND 2/2
}

loadclients() {
	z=""
	for f in "$cliprofile/"*
	do
		if [ -f $f ]
		then
			[ ! -z $z ] && z+="|"
			z+="$(basename $f)|$(cat  "$f")"
		fi
	done
	zenity2array
	clients=("${z[@]}")
	if [ -z "$z" ]
	then
		clients2=("$add" "$add" "" "" "" "")
	else
		clients2=("${z[@]}" "$add" "$add" "" "" "" "" "$del" "$del" "" "" "" "")
	fi
}

function cleanupsrv(){
	kill $vncpid || true
	kill $(cat $srv_stunnel_pid)
}

function cleanupcli(){
	kill $vncpid || true
	kill $(cat $cli_stunnel_pid)
}

#Check dependencies
miss=''
hash "vncviewer" || miss+="xtightvncviewer "
hash "x11vnc" || miss+="x11vnc "
hash "openssl" || miss+="openssl "
hash "curl" || miss+="curl "
hash "fuser" || misee+="psmisc "
if [ ! -z "$miss" ]
then
	zenity $zenity --error --text "Dépendances manquantes : $miss"
	exit 1
fi
unset miss

if [ -z $(ip route show | grep 'default via' | cut -d' ' -f3) ]
then
	zenity $zenity --warning --text "Vous n'êtes pas connecté au réseau"
	exit 1
fi


while true
do
	z=$(zenity $zenity --list \
	--text "Ma situation :" \
	--column "" --column "" --hide-column 1 --hide-header \
	"1" "Je me fais dépanner" \
	"2" "Je dépanne quelqu'un" \
	2>"/dev/null")

	[ $? -ne 0 ] && exit 1
	[ ! -z "$z" ] && break
done

#cli MODE
if [ $z -eq 1 ]
then
	loadclients
	while true
	do
		while true
		do
			z=$(zenity $zenity --list \
			--text "Choix de dépanneur :" \
			--column "File" --hide-column=1 --column "Profil" --column "Contact" --column "Adresse" --column "Port" --column "Clé" \
			"${clients2[@]}" \
			2>"/dev/null")

			[ $? -ne 0 ] && exit 1
			[ ! -z "$z" ] && break
		done
		case "$z" in
			$add)
				while true
				do
					z=$(zenity $zenity --forms \
					--text="$add :" \
					--add-entry "Nom du profil" \
					--add-entry "Telephone (optionnel)" \
					--add-entry "Adresse" \
					--add-entry "Port tunnel TLS" \
					--add-entry "Clé publique (optionnel)" \
					2>"/dev/null")
					[ $? -ne 0 ] && break
					zz="$z"
					zenity2array "$z"
					miss=""
					[ -z ${z[0]} ] && miss+="- Nom du profil\n"
					[ -z ${z[2]} ] && miss+="- Adresse\n"
					[ -z ${z[3]} ] && miss+="- Port tunnel TLS\n"
					if [ ! -z "$miss" ]
					then
						zenity $zenity --warning --text "Echec : ces champs devraient être remplis :\n\n$miss"
					else
						echo "$zz" > $(mktemp -p "$cliprofile")
						unset zz
						loadclients
						break
					fi
				done
			;;
			$del)
				while true
				do
					z=$(zenity $zenity --list \
					--text "$del :" \
					--column "File" --hide-column=1 --column "Profil" --column "Contact" --column "Adresse" --column "Port" --column "Clé" \
					"${clients[@]}" \
					2>"/dev/null")

					[ $? -ne 0 ] && break
					zenity2array "$z"
					if [ ! -z ${z[0]} ]
					then
						rm "$cliprofile/$z"
						loadclients
						break
					fi
				done
				
			;;
			*)
#DO STUFF
				z=$(cat "$cliprofile/$z")
				zenity2array

				echo "\
pid = $cli_stunnel_pid
[cli-VNC]
client = yes
accept = $cli_x11vnc_port
connect = ${z[2]}:${z[3]}" > "$cli_stunnel_cnf"
				[ ! -z "${z[4]}" ] && echo "CAfile = ${z[4]}" >> "$cli_stunnel_cnf"

				fuser -k "$cli_x11vnc_port/tcp" 2>"/dev/null" || true #vncviewer
				fuser -k "$cli_stunnel_cnf/tcp" 2>"/dev/null" || true #stunnel
				trap cleanupcli EXIT

				stunnel "$cli_stunnel_cnf" || { zenity $zenity --error --text "Echec de l'établissement du stunnel"; exit 1; }
				tail --pid=$(cat "$cli_stunnel_pid") -f "/dev/null" &
				tunpid=$!
				while true
				do
					t=$SECONDS
					x11vnc -connect_or_exit "127.0.0.1:$cli_x11vnc_port" &
					vncpid=$!
					wait $vncpid
					if [ $? -ne 0 ]
					then
						zenity $zenity --question --ok-label "Réessayer" --cancel-label "Abandonner" --text "Votre dépanneur ne semble pas disponible ; contactez-le et réessayez !

<sub>Coordonnées utilisées :
	IP dépanneur : ${z[2]}
	Port dépanneur : ${z[3]}</sub>" 2> "/dev/null" &
						zenpid=$!
						wait -n $zenpid $tunpid
						ret=$?
						! kill -s 0 $tunpid > "/dev/null" && { kill $zenpid; zenity --error --text "Le tunnel s'est achevé prématurément"; exit 1; }
						[ $ret -ne 0 ] && exit 1
					else
						wait $vncpid
						break
					fi
				done
				notify-send -i "info" "Fin du contrôle à distance"
				exit 0
			;;
		esac


	done

#SRV MODE
elif [ $z -eq 2 ]
then
	while true
	do
		[  ! -f "$lastport" ] && echo "9999" > "$lastport"
		z=$(zenity --entry $zenity --entry-text $(cat $lastport) \
		--text="Pour pouvoir dépanner vous allez devoir vous rendre accessible :
	* Si vous êtes derrière un NAT vous devrez ajouter une règle adéquate
	* Choisissez un port que vous n'utilisez pas >1024 et <65535

Pour sécuriser la connexion des attaques 'man in the middle' :
	* Copier $cert chez votre 'patient'

Ouvrir un stunnel sur le port :" \
		--add-entry "Port du tunnel TLS" \
		--add-entry "Clé privée (si non indiqué, sera généré)" \
		2>"/dev/null")
		[ $? -ne 0 ] && exit 0
		[ $z -gt 1024 ] 2>"/dev/null" && [ $z -lt 65535 ] && break
	done

	echo "$z" > "$lastport"
#DO STUFF
	echo "\
pid = $srv_stunnel_pid
[srv-VNC]
client = no
cert = $cert
key = $key
accept = $z
connect = 127.0.0.1:$srv_vncviewer_port" > "$srv_stunnel_cnf"

	#Generate/Refresh certificate
	if [ ! -f $cert ] || [ ! -z $(find "$cert" -mtime +$cert_validity -print) ]
	then
		if ! $(openssl req -new -x509 -days $cert_validity -nodes -config "$confssl" -out "$cert" -keyout "$key")
		then
			zenity $zenity --error --text "Echec à la création du certificat" 2>"/dev/null"
			exit 1
		fi
	fi

	#FREE PORT
	fuser -k "$srv_vncviewer_port/tcp" 2>"/dev/null" || true #vncviewer
	fuser -k "$z/tcp" 2>"/dev/null" || true #stunnel
	trap cleanupsrv EXIT

	stunnel "$srv_stunnel_cnf" || { zenity $zenity --error --text "Echec de l'établissement du stunnel"; exit 1; }
	while true; do sleep 0.1; [ -e "$srv_stunnel_pid" ] && break; done #LOOPBACKDANGER
	tail --pid=$(cat "$srv_stunnel_pid") -f "/dev/null" &
	tunpid=$!
	vncviewer -listen $(($srv_vncviewer_port-5500)) &
	vncpid=$!

	zenity $zenity --info --ok-label "Arrêter d'attendre" --text "Attente d'une connexion de l'un de vos 'patients'.

<sub>Coordonnées à donner à votre correspondant :
	Votre IP : $(curl https://ipinfo.io/ip 2>/dev/null) 
	Votre Port : $z
</sub>" 2>"/dev/null" &
	zenpid=$!
	wait -n $tunpid $vncpid $zenpid
	! kill -s 0 $tunpid > "/dev/null" && { kill $zenpid; zenity --error --text "Le tunnel s'est achevé prématurément"; exit 1; }
	! kill -s 0 $vncpid > "/dev/null" && { kill $zenpid; zenity --error --text "Le serveur vncviewer s'est achevé prématurément"; exit 1; }
	exit 0
fi