#Proxy system to privoxy
echo "http_proxy=http://127.0.0.1:8118" >> "/etc/environment"

#.onion goes to tor
echo "forward-socks5t .onion 127.0.0.1:9050 ." >> "/etc/privoxy/config"

#Fix : privoxy only need to wait for network
sed -r 's|^(WantedBy=).*|\1network.target|' "/lib/systemd/system/privoxy.service" > '/etc/systemd/system/privoxy.service'

systemctl reenable privoxy

#d='/etc/systemd/system/privoxy.service.d'
#mkdir -p "$d"
#echo "[Install]
#WantedBy=network-online.target
#" > "$d/wait4network.conf"

#activer TPB en onion
sed -ri 's|https://thepiratebay.org/|http://piratebayztemzmv.onion/|' '/etc/skel/.mozilla/firefox/main.default-esr/places.sqlite' '/etc/skel/.dillo/dillorc' '/etc/skel/.config/falkon/profiles/default/bookmarks.json'
