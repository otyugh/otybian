echo "[net/launchpad/plank/docks/dock1]
icon-size=48
show-dock-item=false
position='left'
dock-items=['firefox-esr.dockitem', 'libreoffice-writer.dockitem', 'libreoffice-impress.dockitem', 'scratch.dockitem', 'scribus.dockitem']
unhide-delay=0
items-alignment='center'
theme='Default'
hide-mode='none'
pinned-only=false
auto-pinning=true
alignment='center'
zoom-percent=150
hide-delay=0
zoom-enabled=false
monitor=''
lock-items=true
tooltips-enabled=true
pressure-reveal=false
offset=0
current-workspace-only=false
" >> '/etc/dconf/db/site.d/plank'
