#Cacher la catégorie script
echo "NoDisplay=true" >> "/usr/share/otybian/desktop-directories/live-script.directory"

#Màj automatique
echo 'APT::Periodic::Enable "1";
APT::Periodic::Update-Package-Lists "3";
APT::Periodic::Download-Upgradeable-Packages "1";
APT::Periodic::Unattended-Upgrade "1";
APT::Periodic::AutocleanInterval "7";
APT::Periodic::MaxAge "30";
APT::Periodic::MinAge "2";
APT::Periodic::MaxSize "500";
APT::Periodic::RandomSleep "36000";
' > "/etc/apt/apt.conf.d/10periodic"

#Firefox alternate
mv "/etc/skel/.mozilla/firefox/main.default-esr" "/etc/skel/.mozilla/firefox/main.default-esr.bak"
mv "/etc/skel/.mozilla/firefox/main2.default-esr" "/etc/skel/.mozilla/firefox/main.default-esr"