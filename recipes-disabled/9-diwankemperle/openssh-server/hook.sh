#Ajouter ma clé d'accès
conf='/etc/ssh/sshd_config'

#Ne pas autoriser le mot de passe via ssh
echo "PasswordAuthentication no" >> "$conf"
echo "ChallengeResponseAuthentication no" >> "$conf"

#eviter les 100ms/60min d'attente de sshd pour lancer l'interface graphique
sed -r 's|^(WantedBy=).*|\1network-online.target|' '/lib/systemd/system/ssh.service' > '/etc/systemd/system/ssh.service'
