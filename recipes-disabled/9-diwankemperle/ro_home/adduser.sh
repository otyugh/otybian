#chmod u-r "$HOME"
rmdir --ignore-fail-on-non-empty "$HOME/"*

wall="$HOME/Bureau/Drekleurioù/egor.jpg"
cp "$WALLPAPER" "$wall"
chown $USER:$USER "$wall"

f="$HOME/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-desktop.xml"
[ -f "$f" ] && sed -ri "s|$WALLPAPER|$wall|" "$f"

rm -f "$HOME/.config/gtk-3.0/bookmarks"
