mkdir -p '/etc/default'
echo 'LANG=br_FR.UTF-8' > '/etc/default/locale'

#Breton default in libreoffice
sed -ri 's|(<item oor:path="/org.openoffice.Office.Linguistic/General"><prop oor:name="DefaultLocale" oor:op="fuse"><value>)[^<]*(</value></prop></item>)|\1br-FR\2|' '/etc/skel/.config/libreoffice/4/user/registrymodifications.xcu'

