find "/usr/share/otybian/applications/script/" -type f -print0 | while read -d '' f
do
	cp "$f" "/usr/share/applications/"
done

echo "* custom .desktop directories"
find "/usr/share/otybian/desktop-directories/" -type f -print0 | while read -d '' f
do
	cp "$f" "/usr/share/desktop-directories/"
done