#!/bin/bash

if [ $(id -u) -ne 0 ]
then
	#hack : l'attente permet de laisser le temps au terminal de s'ouvrir avant pkexec
	xfce4-terminal -e "sh -c 'sleep 0.5; pkexec $0;'"
	exit 0
fi


function echo2(){ echo -e "$(echo "$@" | sed -r 's|\*([^\*]*)\*|\\033[1m\1\\033[0m|g')"; }

function separator() {
	echo -e "######################################################################"
}

dist=$(lsb_release -cs)
src="/etc/apt/sources.list"

function is_enabled() {
	grep -Eqm1 "^deb .* $dist .* non-free.*" "$src"
	return $?
}

function enable() {
	sed -ri "s|^(deb .* $dist )(.*)$|\1\2 non-free|" "$src"
	echo2 "=> Mirroir privateur *activé*"
	echo2 "=$ *apt update* : rafraîchissement de la liste des paquets"	
	apt update
}

function disable() {
	sed -ri "s|^(deb .* $dist )(.*)non-free?(.*)()$|\1\2\3|" "$src"
	echo2 "=> Mirroir privateur *désactivé*"
	echo2 "=$ *apt update* : rafraîchissement de la liste des paquets"	
	apt update
}

function hwbrand() {
	case "$1" in
		10de) echo 'NVIDIA';;
		1002) echo 'AMD/ATI';;
		1022) echo 'AMD';;
		1969|168c) echo 'Atheros';;
		8086) echo 'Intel';;
		13f6) echo 'C-Media';;
		104a) echo 'STMicoemectronics';;
		10ec) echo 'Realtec';;
		#
		104c) echo 'Texas Instruments';;
		167b|2116) echo 'ZyDAS';;
		1804|1814) echo 'Ralink';;
		1fc1|1077) echo 'QLogic';;
		4040) echo 'NetXen';;
		14c1) echo 'Myricom';;
		11ab|1b4b) echo 'Marvell';;
		13a0) echo 'Crystal';;
		1657|177d) echo 'Cavium';;
		182f|166d|173b|feda|1000|1166|14e4) echo 'Broadcom';;
		11d4) echo 'Analog Devices';;
		*) echo '???';;
	esac
}

function installd() {
	wat=$1
	brand=$(hwbrand $2)
	name=$3
	firmware=($4)
	firmwaren=${#firmware[@]}

	if [ -n "$name" ]
	then
		echo2 "$wat $brand : *$name*"
		if [ "$firmware" != "N/A" ]
		then
			i=0
			while [ $i -lt $firmwaren ]
			do
				if dpkg -s $firmware 2>'/dev/null' | grep -q "^Status: install ok installed"
				then
					break
				fi
				let i++
			done
			if [ $i -ne $firmwaren ]
			then
				echo2 "Firmware suggéré : *${firmware[$a]}* (déjà installé)"
			else
				i=0
				while [ $i -ne $firmwaren ]
				do
					echo2 "Firmware suggéré ($(($i+1))/$firmwaren) : *${firmware[$i]}*"
					read -p "Installer ? [o/N]" a
					if echo "$a" | grep -q "^[o|O]"
					then
						d+="${firmware[$i]} "
						echo "Sera installé"
						break
					fi
					let i++
				done
				
			fi
		else
			echo2 "Aucun firmware à proposer"
		fi
	fi
	echo ""
}

function hwfirmware() {
	cpu=$(lspci -vmmnd ::0600 | grep -m1 "^Vendor:" | cut -d$'\t' -f2)
	gpu=$(lspci -vmmnd ::0300 | grep -m1 "^Vendor:" | cut -d$'\t' -f2)
	eth=$(lspci -vmmnd ::0200 | grep -m1 "^Vendor:" | cut -d$'\t' -f2)
	wla=$(lspci -vmmnd ::0280 | grep -m1 "^Vendor:" | cut -d$'\t' -f2)
	snd=$(lspci -vmmnd ::0403 | grep -m1 "^Vendor:" | cut -d$'\t' -f2)

	cpun=$(lspci -vmmd ::0600 | grep -m1 "^Device:" | cut -d$'\t' -f2)
	gpun=$(lspci -vmmd ::0300 | grep -m1 "^Device:" | cut -d$'\t' -f2)
	ethn=$(lspci -vmmd ::0200 | grep -m1 "^Device:" | cut -d$'\t' -f2)
	wlan=$(lspci -vmmd ::0280 | grep -m1 "^Device:" | cut -d$'\t' -f2)
	sndn=$(lspci -vmmd ::0403 | grep -m1 "^Device:" | cut -d$'\t' -f2)

	case $cpu in
		1022) cpud="amd64-microcode";;
		8086) cpud="intel-microcode";;
		*) cpud="N/A"
	esac

	case $gpu in
		1002) gpud="firmware-amd-graphics";;
		8086) gpud="firmware-misc-nonfree";;
		10de) gpud="nvidia-driver nvidia-legacy-340xx-driver nvidia-legacy-304xx-driver";;
		#
		13a0) gpud="firmware-crystalhd";;
		*) gpud="N/A"
	esac

	case $eth in
		1969) ethd="N/A" ;;
		10ec) ethd="firmware-realtek";;
		#
		4040) ethd="firmware-netxen";;
		1fc1|1077) ethd="firmware-qlogic";;
		14c1) ethd="firmware-myricom";;
		1657|177d) ethd="firmware-cavium";;
		11d4) ethd="firmware-misc-nonfree";;
		*) ethd="N/A"
	esac

	case $wla in
		168c) wlad="firmware-atheros";;
		8086) wlad="firmware-iwlwifi firmware-ipw2x00 firmware-intelwimax";;
		#
		167b|2116) wlad="zd1211-firmware";;
		104c) wlad="firmware-ti-connectivity";;
		11ab|1b4b) wlad="firmware-libertas";;
		182f|166d|173b|feda|1000|1166|14e4) wlad="firmware-brcm80211 firmware-bnx2 firmware-bnx2x firmware-b43legacy-installer firmware-b43-installer";;
		*) wlad="N/A"
	esac

	case $snd in
		8086|1002|13f6) sndd="firmware-intel-sound";;
		*) sndd="N/A"
	esac

d=""
installd "Processeur" "$cpu" "$cpun" "$cpud"
installd "Carte graphique" "$gpu" "$gpun" "$gpud"
installd "Carte réseau" "$eth" "$ethn" "$ethd"
installd "Carte wifi" "$wla" "$wlan" "$wlad"
installd "Carte son" "$snd" "$sndn" "$sndd"

if [ -n "$d" ]
then
	echo2 "*apt install $d*"
	apt install $d
fi

}

#function usefulnonfree(){
	#echo2 "*unrar* : permet l'accès à certaines archives (.rar) et type de fichier (.cbr, .cbz)"
	#echo2 "*ttf-mscorefonts-installer* : polices contenant arial, comic sans, times new roman, verdana..."
	#echo2 "*pepperflashplugin-nonfree* : permet l'execution de flash dans chromium (risque de sécurité)"
#}

function uninstall() {
	a=$(dpkg-query -W -f='${db:Status-Abbrev}${Section}\t${Package}\n' | grep "^ii non-free" | cut -d$'\t' -f2 | tr "\n" " ")
	if [ $(echo $a | wc -w) -gt 0 ]
	then
		echo2 "*apt-get purge $a*"
		apt purge $a
	else
		echo2 "Aucun paquet privateur installé ! :)"
	fi
}

while true
do
	if is_enabled
	then
		state="activé"
		action="Désactiver"
	else
		state="désactivé"
		action="Activer"
	fi

	separator
	echo2 "*Gérer vos firmwares privateur*"
	separator
	echo2 "\
*1* - $action le mirroir privateur (actuellement *$state*)
*2* - Suggérer une liste de firmwares privateur (à partir de la marque)
*3* - Désinstaller tous les logiciels privateurs ($(dpkg-query -W -f='${db:Status-Abbrev}${Section}\t${Package}\n' | grep '^ii non-free' | wc -l))
*4* - Quitter"

	while echo "$a" | grep -qv "[1-4]"
	do
		read a
	done

	case $a in
		1) if [ "$state" == "activé" ]; then disable; else enable; fi;;
		2) if [ "$state" == "activé" ]; then hwfirmware; else echo2 "*Activez d'abord votre mirroir privateur !*"; fi;;
		3) uninstall;;
		4) exit 0;;
	esac
	unset a
done
