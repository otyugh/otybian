echo "* Save apt update + nonfree"

dist=$(lsb_release -cs)
src="/etc/apt/sources.list"
sed -ri "s|^(deb .* $dist )(.*)$|\1\2 non-free|" "$src"
apt-get update

mkdir -p '/usr/share/otybian/'
cp -r '/var/lib/apt/lists' '/usr/share/otybian/lists'

mkdir -p '/usr/share/otybian/nonfree'
a="$PWD"
cd '/usr/share/otybian/nonfree'
apt download firmware-realtek firmware-qlogic firmware-myricom firmware-cavium firmware-misc-nonfree firmware-atheros firmware-iwlwifi firmware-intelwimax firmware-libertas firmware-brcm80211 firmware-bnx2 firmware-bnx2x firmware-b43legacy-installer firmware-b43-installer
cd "$PWD"
