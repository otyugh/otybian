conf="$chrootP/etc/skel/.config/Thunar/uca.xml"
sed -i '$ d' "$conf"
echo "<action>
	<icon>pdfshuffler</icon>
	<name>Optimiser le PDF</name>
	<unique-id>1572134025687885-1</unique-id>
	<command>ps2pdf %f</command>
	<description>Réduire la taille d&apos;un pdf</description>
	<patterns>*.pdf</patterns>
	<other-files/>
</action>
</actions>" >> "$conf"
