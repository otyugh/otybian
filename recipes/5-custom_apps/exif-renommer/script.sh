conf="$chrootP/etc/skel/.config/Thunar/uca.xml"
sed -i '$ d' "$conf"
echo "<action>
        <icon>time-admin</icon>
        <name>Renommer par date de prise</name>
        <unique-id>1586046887622543-1</unique-id>
        <command>jhead -autorot -nf%%Y-%%m-%%d_%%H-%%M-%%S %F</command>
        <description>Exploite la date EXIF du fichier</description>
        <patterns>*</patterns>
        <image-files/>
</action>
</actions>" >> "$conf"
