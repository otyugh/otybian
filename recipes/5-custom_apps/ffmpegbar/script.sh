conf="$chrootP/etc/skel/.config/Thunar/uca.xml"
sed -i '$ d' "$conf"
echo "<action>
	<icon>gtk-convert</icon>
	<name>Convertir en mp3</name>
	<unique-id>1561459818275563-2</unique-id>
	<command>ffmpegbar -e mp3 %F</command>
	<description>Extraire l'audio</description>
	<patterns>*</patterns>
	<audio-files/>
	<video-files/>
</action>
<action>
	<icon>gtk-convert</icon>
	<name>Convertir en mp4 (720p)</name>
	<unique-id>1561459828275563-2</unique-id>
	<command>ffmpegbar -e mp4 -a \"-map 0 -vf scale=-2:720 -r 25 -c:v libx264 -preset medium -crf 30 -c:a libvorbis -q:a 3 -c:s copy\" %F</command>
	<description>Normaliser la qualité de la vidéo</description>
	<patterns>*</patterns>
	<video-files/>
</action>
</actions>" >> "$conf"
