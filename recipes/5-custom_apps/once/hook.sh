echo "* Apply once hack on a few .desktop"
#desktop files
for f in "dillo" "hexchat" "simple-scan" "pavucontrol" "grsync" "firefox-esr" "lxtask" "virtualbox" "scratch" "org.kde.falkon" "mate-screenshot" "grsync" "xfce-display-settings"
do
	f="/usr/share/otybian/applications/${f}.desktop"
	[ -f "$f" ] && sed -ri "s|(^Exec=)(.*)|\1once \2|" "$f"
done