apt-get -y purge gnome-packagekit gnome-packagekit-data

xdg-desktop-menu install "/usr/share/desktop-directories/live-script.directory" "/usr/share/otybian/applications/script/update-sh.desktop"

mkdir -p "/etc/skel/.config/package-update-indicator"
echo "[General]
update-command='update.sh'
use-mobile-connection=false
" > "/etc/skel/.config/package-update-indicator/package-update-indicator.conf"
