#!/bin/bash
if [ $(id -u) -ne 0 ]
then
	#hack : l'attente permet de laisser le temps au terminal de s'ouvrir avant pkexec
	xfce4-terminal -e "sh -c 'sleep 0.5; pkexec $0;'"
	exit 0
fi

function echo2(){ echo -e "$(echo "$@" | sed -r 's|\*([^\*]*)\*|\\033[1m\1\\033[0m|g')"; }
function echo2g(){ echo -e "$(echo "\033[32m${@}\033[0m" | sed -r 's|\*([^\*]*)\*|\\033[1m\1\\033[0m\\033[32m|g')"; }
function echo2r(){ echo -e "$(echo "\033[33m${@}\033[0m" | sed -r 's|\*([^\*]*)\*|\\033[1m\1\\033[0m\\033[33m|g')"; }
function exit2(){ echo2 "Appuyez sur une touche ou fermez la fenêtre."; read -n1; exit $1; }

function separator() {
	echo -e "\n######################################################################\n"
}

function locked() {
	pid=$(timeout 5 lsof -t "/var/lib/dpkg/lock")
	r=$?
	if [ $r -eq 0 ]
	then
		proc=$(ps -p $pid -o command=)
		if echo "$proc" | grep -E "packagekitd$"
		then
			if pgrep gpk-update-viewer
			then
				proc="gpk-update (\"Outil de Màj de paquet\")"
			elif pgrep gnome-software 1>"/dev/null"
			then
				proc="gnome-software (\"Logiciel\")"
			else
				proc="Un utilitaire utilisant packagekit"
			fi
		fi

		echo2 "\
Des (dés)installations sont en cours.
Patientez ou fermez-le logiciel bloquant la mise à jour.
Dans le pire des cas, redémarrez.

(Attente de la fermeture de \"*${proc}*\"...)"
		#wait $b
		while lsof -t "/var/lib/dpkg/lock" >"/dev/null"; do sleep 3; done

		echo "Gestionnaire déverrouillé ! Reprise."
		separator
	elif [ $r -eq 124 ]
	then
		echo2r "(Avertissement) lsof ne répond pas (cause commune : point de montage inaccessible)"
	fi
}

function interrupt() {
	r=$?
	out="Erreur $r !"
	if echo "$cmd" | grep -q "^apt-get"
	then
		case "$r" in
			100) out="Pas de connexion internet (ou serveurs mirroirs indisponibles)" ;;
			130) out="Mise à jour interrompue de force" ;;
		esac
	fi
	separator
	echo2r "*Interruption du programme.*\n$out"
	exit2 1
}


function askndo() {
	locked
	echo2 "*${cmd}* : ${dsc}"
	#read -p "Entrée pour lancer"
	sleep 1
	echo "$cmd" | grep -q "^apt-get" && cmd="$cmd -y"
	$cmd || interrupt
	echo2 "*Fait.*"
	separator
}

echo2 "*Script de mise à jour*"
#-->Appuyez sur entrée (⏎)<--
#read arg
separator

cmd="dpkg --configure -a"
dsc="cherche des paquets non-configurés"
askndo

cmd="apt-get install -f"
dsc="vérifie les paquets/dépendances cassées"
askndo


cmd="apt-get update"
dsc="cherche les dernières mises à jour"
#We don't have to update every time !
toolate=$(date -d "yesterday" +%s)
[ -e "/var/lib/apt/periodic/update-success-stamp" ] && lastupd=$(date -r "/var/lib/apt/periodic/update-success-stamp" +%s) || lastupd=0
lastmod=$(date -r "/etc/apt/sources.list" +%s)
if [ $toolate -gt $lastupd ] || [ $lastmod -gt $lastupd ]
then
	askndo
else
	echo2 "Cache de moins de 24h - passe outre *${cmd}*".
	separator
	sleep 1
fi

cmd="apt-get dist-upgrade"
dsc="télécharge et applique les mises à jour"
askndo

cmd="apt-get autoremove"
dsc="supprime les paquets orphelins"
askndo

#hack : pour les gens utilisant package-update-indicator
if pid=$(pgrep "package-update-")
then
	usr=$(ps -p $pid -o user=)
	kill $pid
	su -l "$usr" -c "export DISPLAY=:0.0; package-update-indicator 2>'/dev/null' &"
	#disown $!
	#pkcon refresh &
fi

echo2g "Votre *$(lsb_release -ds)* est à jour !"
exit2 0
