echo \
'    --mode "debian" \
    --system "live" \
    --archive-areas "main" \
    --security "true" \
    --updates "true" \
    --backports "false" \
    --binary-filesystem "fat32" \
    --binary-images "iso-hybrid" \
    --apt-indices "true" \
    --apt-recommends "true" \
    --apt-secure "true" \
    --apt-source-archives "false" \
    --firmware-binary "true" \
    --firmware-chroot "true" \
    --win32-loader "false" \
    --memtest "memtest86" \
    --clean \
    --debug \
    --verbose \
    --source "false" \' >> "$configP"


installer_logo="$livedir/config/includes.installer/usr/share/graphics/"
mkdir -p "$installer_logo"
cp "$recette/logo_debian.png" "$installer_logo"
