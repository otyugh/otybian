## Bootloader live personnalisé

Tout ce qui concerne l'écran de démarrage quand vous démarrez sur l'ISO

- Ajout/Préconfiguration d'un menu Grub (en mode EFI) et Syslinux (en mode Legacy)
- Outils de diagnostique HDT & memtest (dans Syslinux seulement)
- 5 profil d'installation ("preseed") : expert, classic, classic efi, auto, auto-efi
