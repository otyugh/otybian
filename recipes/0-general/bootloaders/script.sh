#Download the last daily pci.ids for HDT
curl2 "https://pci-ids.ucw.cz/v2.2/pci.ids" "$livedir/config/includes.binary/isolinux/pci.ids"

echo \
'    --debian-installer "live" \
    --debian-installer-gui "true" \
    --memtest "memtest86" \' >> "$configP"

for a in "$recette/bootloader/seed/"*
do
	sed -ri "s|label\{ debian \}|label\{ debian_${FLAVOUR}-${ARCHITECTURE} \}|" "$a"
done
