## Divers

Modification système qui ne méritaient pas une entrée à elles seules

- Activation des "magic keys"
- Forcer FSCK chaque mois
- Désactiver les beeps clavier
- Monter des disques durs locaux sans demander le mot de passe admin
- Coupe la WIFI au démarrage si sur batterie (avec TLP)
- Baisser le nombre de i/o en vidant le buffer moins souvent (commit=60)
- Limiter le nombre de refresh NTP (toutes les heures ça suffit)
