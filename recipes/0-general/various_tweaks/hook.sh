echo "* Enable magic keys"
sed -ri 's/#(kernel.sysrq=1)/\1/' '/etc/sysctl.conf'
sysctl -p 1>'/dev/null'

echo "* Force fsck every month"
echo "#!/bin/sh\ntouch /forcefsck /home/forcefsck" > '/etc/cron.monthly/forcefsck'

echo "* Disable annoying beeps"
echo "blacklist pcspkr" > '/etc/modprobe.d/nobeep.conf'

echo "* Allow mounting drives without password"
f="/usr/share/polkit-1/actions/org.freedesktop.udisks2.policy"
[ -f "$f" ] && sed -ri "s|(<allow_active>)auth_admin_keep(</allow_active>)|\1yes\2|" "$f"

echo "* Shut wifi if booting from battery && restore devices state on startup"
sed -ri "s/^(WIFI_PWR_ON_BAT=).*/\1off/;s/^(RESTORE_DEVICE_STATE_ON_STARTUP=).*/\11/" "/etc/default/tlp"

echo "* Set root's binary path (useful since buster)"
echo '[ $USER != "root" ] && PATH+=":/usr/sbin:/sbin"' >> "/root/.bashrc"

echo "* Delete unwanted recommended packages"
apt-get -y purge exim4-config libunbound8 libgnutls-dane0

#echo "* Set reasonable limits to NTP refresh"
#sed -ri "s|^(pool .* iburst)$|\1 minpoll 12 maxpoll 17|" "/etc/ntp.conf"

