########
#Naming
########
echo \
'    --iso-application "'$ISONAME'" \
    --iso-volume "'$ISONAME'" \
    --iso-publisher "'$ISOPUBLISHER'" \'  >> "$configP"

title="${ISONAME} ${ARCHITECTURE} (${FLAVOUR}) build du $(date +%F)"
sed -ri "s|^(menu title ).*|\1$title|" "$bootloaderP/isolinux/menu.cfg"
sed -ri "s|^(title-text: \").*|\1$title\"|" "$bootloaderP/boot/grub/theme.txt"

#version's name
sed -ri "s|Debian VERSION|Debian $FLAVOUR|" "$bootloaderP/isolinux/menu.cfg"
sed -ri "s|Debian VERSION|Debian $FLAVOUR|" "$bootloaderP/boot/grub/grub.cfg"

#########
#Flavour
#########
echo \
'    --distribution "buster" \
    --debian-installer-distribution "daily" \' >> "$configP"

#############
#Architecture
#############
case $ARCHITECTURE in
64)
	echo '    -a "amd64" --linux-flavours "amd64" \' >> "$configP"
	[ -d "$chrootP/usr/lib/i386-linux-gnu/" ] && lb clean --purge

	#https for APT
	sed -ri 's|^deb http://|deb https://|' "$adduserP"
;;
32)
	echo '    -a "i386" -k "686" --linux-flavours "686-pae" \' >> "$configP"
	[ -d "$chrootP/usr/lib/x86_64-linux-gnu/" ] && lb clean --purge
	sed -ri 's|x86_64-linux-gnu|i386-linux-gnu|' "$chrootP/etc/skel/.local/share/applications/Thunar-bulk-rename.desktop"
;;
*)
	echo "ERR : architecture no recognized"
	exit 1
;;
esac
