echo "* Set apt source"
echo "deb https://deb.debian.org/debian $FLAVOUR main
deb https://deb.debian.org/debian-security $FLAVOUR/updates main
deb https://deb.debian.org/debian $FLAVOUR-updates main

#Debian backports : https://backports.debian.org/
#deb https://deb.debian.org/debian $FLAVOUR-backports main

#Le projet videolan : https://www.videolan.org/developers/libdvdcss.html
#deb https://download.videolan.org/pub/debian/stable/ /" > '/etc/apt/sources.list'
