plymouth-set-default-theme spinfinity
sed -ri 's|^(GRUB_CMDLINE_LINUX_DEFAULT=).*|\1"splash"|' "/etc/default/grub"

#Menu will appear if you press and hold Shift during loading Grub, if you boot using BIOS. When your system boots using UEFI, press Esc.
sed -ri 's|(GRUB_TIMEOUT=).*|\10|' "/etc/default/grub"
