## Fonds d'écrans

- Une dizaine de fond d'écrans dans /usr/share/backgrounds/
- L'un d'eux est selectionné par défaut pour grub, lightdm, xfdesktop
- Si l'on démarre en mode live, il sera écrit "Session live" dessus
