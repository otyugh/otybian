#xfdesktop
f="/etc/skel/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-desktop.xml"
[ -f "$f" ] && sed -ri 's|( *<property name="last-image" type="string" value=")[^"]+("/>)|\1'$WALLPAPER'\2|' "$f"

#lightdm
f="/etc/lightdm/lightdm-gtk-greeter.conf"
[ -f "$f" ] && sed -ri "s|^#(background=).*|\1$WALLPAPER|" "$f"

#remove buggy wallpapers
rm -f "/usr/share/backgrounds/"*".svg"