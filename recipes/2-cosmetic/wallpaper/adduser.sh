if [ $uid -eq 1000 ]
then
	if [ $(df '/' --output=source | tail -n1) == 'overlay' ]; then
		echo "* [Live] Set custom live wallpapers"
		txt=$(mktemp --suffix ".gif")
		convert -transparent black -background black -fill white -pointsize 80 label:Live-Session "$txt"
		#find "/usr/share/backgrounds" -maxdepth 1 -type f -name "*.jpg" -print0 | while read -d '' file
		#do
		#	tmp=$(mktemp --suffix ".jpg")
		#	composite -geometry +0+0 -gravity center "$txt" "$file" "$tmp"
		#	mv "$tmp" "$file"
		#	chmod ugo+r "$file"
		#done
		tmp=$(mktemp --suffix ".jpg")
		composite -geometry +0+0 -gravity center "$txt" "$WALLPAPER" "$tmp"
		mv "$tmp" "$WALLPAPER"
		chmod ugo+r "$WALLPAPER"

		rm "$txt"
	fi
fi