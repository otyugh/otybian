#déclinaisons
f="/usr/share/tmp/system-software-update.png"
for dim in 96 64 48 32 24 22 16
do
	convert "$f" -resize "${dim}x${dim}" "/usr/share/icons/elementary-xfce/apps/${dim}/system-software-update.png"
done
mv "$f" "/usr/share/icons/elementary-xfce/apps/128/system-software-update.png"