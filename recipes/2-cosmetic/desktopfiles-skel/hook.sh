echo "* Auto-update .desktop after apt install"
echo 'DPkg::Post-Invoke {"/usr/local/bin/custom-desktop || true";};' >> '/etc/apt/apt.conf.d/99hack'

/usr/local/bin/custom-desktop

#Bug (léger décalage) in whisker if not hidden
echo "* Hide xfce-screensavers category"
echo "NoDisplay=true" >> "/usr/share/desktop-directories/xfce-screensavers.directory"

echo "* Dissocier (mime) firefox des images"
rm -f "/usr/lib/mime/packages/firefox-esr"
update-mime
