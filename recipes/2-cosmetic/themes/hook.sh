echo "* Setting icon cache"
gtk-update-icon-cache '/usr/share/icons/Adwaita/'
gtk-update-icon-cache '/usr/share/icons/gnome/'
gtk-update-icon-cache '/usr/share/icons/HighContrast/'

gtk-update-icon-cache '/usr/share/icons/elementary-xfce/'
gtk-update-icon-cache '/usr/share/icons/elementary-xfce-dark/'
gtk-update-icon-cache '/usr/share/icons/elementary-xfce-darker/'
gtk-update-icon-cache '/usr/share/icons/elementary-xfce-darkest/'

#gtk-update-icon-cache '/usr/share/icons/elementaryXubuntu-dark/'
gtk-update-icon-cache '/usr/share/icons/breeze/'
gtk-update-icon-cache '/usr/share/icons/breeze-dark/'
#gtk-update-icon-cache '/usr/share/icons/oxygen/'

echo "* Set default theme and icon theme"
sed -ri "s|^#(theme-name = ).*|\1$THEME|" '/etc/lightdm/lightdm-gtk-greeter.conf'
sed -ri "s|^#(icon-theme-name = ).*|\1$THEME_ICON|" '/etc/lightdm/lightdm-gtk-greeter.conf'
sed -ri "s|^( +<property name=\"ThemeName\" type=\"string\" value=\").*(\"/>)|\1$THEME\2|" '/etc/skel/.config/xfce4/xfconf/xfce-perchannel-xml/xsettings.xml'
sed -ri "s|^( +<property name=\"IconThemeName\" type=\"string\" value=\").*(\"/>)|\1$THEME_ICON\2|" '/etc/skel/.config/xfce4/xfconf/xfce-perchannel-xml/xsettings.xml'
sed -ri "s|^(    <property name=\"theme\" type=\"string\" value=\").*(\"/>)|\1$THEME_XFWM\2|" '/etc/skel/.config/xfce4/xfconf/xfce-perchannel-xml/xfwm4.xml'

echo "* Harmoniser thèmes gtk/qt (avec qt5-style-plugins)"
echo "QT_QPA_PLATFORMTHEME=gtk2" >> "/etc/environment"

echo "* Ne jamais cacher la barre de défilement dans Gnome"
echo "GTK_OVERLAY_SCROLLING=0" >> "/etc/environment"
