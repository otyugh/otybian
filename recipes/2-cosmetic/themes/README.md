## Installation/Initialisation de quelques themes

- Ajout de thème : greybird, arc
- Ajout de thème custom : ARK, icy
- Initialisation du cache des icônes
- Personnalisation du thème selectionné par défaut des fenêtres et des icônes
- Harmonisation des thèmes qt/gtk
