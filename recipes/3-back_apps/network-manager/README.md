## Network-manager

- DNS de FDN && LDN par défaut dans le premier profil de connexion
- Ne pas attendre le réseau au démarrage (boot plus rapide)
- Ne pas afficher les notifications "connexion/déconnexion" (y a déjà un icône indicateur)
