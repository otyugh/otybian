echo "* Disable waiting networkmanager"
systemctl disable 'NetworkManager-wait-online.service'

echo "\
[org/gnome/nm-applet]
disable-disconnected-notifications=true
disable-connected-notifications=true
" > '/etc/dconf/db/site.d/network-manager'