echo "* Disabling pulseaudio's flat volume (binds system volume to the highest app volume)"
sed -ri 's|; (flat-volumes =) yes|\1 no|' '/etc/pulse/daemon.conf'