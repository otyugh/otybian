echo "* Workaround to get newer xfdesktop"
#xfdesktop4 buster break thunar's custom actions
#xfwm4 buster make grey background at bootup
source="bullseye"

#Goto frankendebian
echo "deb http://deb.debian.org/debian $source main" >> '/etc/apt/sources.list'

#Only maintain xfdesktop/xfwm to date
echo "Package: *
Pin: release n=$source
Pin-Priority: 100

Package: xfdesktop4 xfwm4
Pin: release n=$source
Pin-Priority: 500
" > "/etc/apt/preferences"

apt-get update
apt-get -y -t $source install xfdesktop4 xfwm4



