#!/bin/bash
torrentid="$1"
torrentname="$2"
torrentpath="$3"

source "$HOME/.config/user-dirs.dirs"

ln -s "$torrentpath" "$XDG_DOWNLOAD_DIR/$torrentname"
