echo "* Deluge's config"

f="$HOME/.config/deluge/gtkui.conf"
[ -f "$f" ] && sed -ri "s|(^  \"ntf_sound_path\": \").[^\"]*(\", )|\1$XDG_DOWNLOAD_DIR\2|" "$f"
[ -f "$f" ] && sed -ri "s|(^  \"choose_directory_dialog_path\": \").[^\"]*(\", )|\1$XDG_DOWNLOAD_DIR\2|" "$f"

f="$HOME/.config/deluge/core.conf"
[ -f "$f" ] && sed -ri "s|(^  \"move_completed_path\": \").[^\"]*(\", )|\1$XDG_TORRENT_DIR\2|" "$f"
[ -f "$f" ] && sed -ri "s|(^  \"torrentfiles_location\": \").[^\"]*(\", )|\1$XDG_TORRENT_DIR\2|" "$f"
[ -f "$f" ] && sed -ri "s|(^  \"download_location\": \").[^\"]*(\", )|\1$XDG_TORRENT_DIR\2|" "$f"
[ -f "$f" ] && sed -ri "s|(^  \"autoadd_location\": \").[^\"]*(\", )|\1$XDG_TORRENT_DIR\2|" "$f"
[ -f "$f" ] && sed -ri "s|(^  \"plugins_location\": \").[^\"]*(\", )|\1$HOME/.config/deluge/plugins\2|" "$f"
