[fwBasic]
status = disabled
incoming = deny
outgoing = deny
routed = disabled

[Rule0]
ufw_rule = 123/udp ALLOW OUT Anywhere (out)
description = Protocole d'heure réseau (NTP)
command = /usr/sbin/ufw allow out proto udp from any to any port 123
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 123/udp
iface = 
routed = 
logging = 

[Rule1]
ufw_rule = 80/tcp ALLOW OUT Anywhere (out)
description = HTTP
command = /usr/sbin/ufw allow out proto tcp from any to any port 80
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 80/tcp
iface = 
routed = 
logging = 

[Rule2]
ufw_rule = 443/tcp ALLOW OUT Anywhere (out)
description = HTTPS
command = /usr/sbin/ufw allow out proto tcp from any to any port 443
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 443/tcp
iface = 
routed = 
logging = 

[Rule3]
ufw_rule = 6667/tcp ALLOW OUT Anywhere (out)
description = IRC
command = /usr/sbin/ufw allow out proto tcp from any to any port 6667
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 6667/tcp
iface = 
routed = 
logging = 

[Rule4]
ufw_rule = 6697/tcp ALLOW OUT Anywhere (out)
description = IRC SSL
command = /usr/sbin/ufw allow out proto tcp from any to any port 6697
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 6697/tcp
iface = 
routed = 
logging = 

[Rule5]
ufw_rule = 143/tcp ALLOW OUT Anywhere (out)
description = IMAP
command = /usr/sbin/ufw allow out proto tcp from any to any port 143
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 143/tcp
iface = 
routed = 
logging = 

[Rule6]
ufw_rule = 6881:6891/tcp ALLOW OUT Anywhere (out)
description = Deluge Torrent
command = /usr/sbin/ufw allow out proto tcp from any to any port 6881:6891
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 6881:6891/tcp
iface = 
routed = 
logging = 

[Rule7]
ufw_rule = 6881:6891/udp ALLOW OUT Anywhere (out)
description = Deluge Torrent
command = /usr/sbin/ufw allow out proto udp from any to any port 6881:6891
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 6881:6891/udp
iface = 
routed = 
logging = 

[Rule8]
ufw_rule = 110/tcp ALLOW OUT Anywhere (out)
description = POP3
command = /usr/sbin/ufw allow out proto tcp from any to any port 110
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 110/tcp
iface = 
routed = 
logging = 

[Rule9]
ufw_rule = 25/tcp ALLOW OUT Anywhere (out)
description = SMTP
command = /usr/sbin/ufw allow out proto tcp from any to any port 25
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 25/tcp
iface = 
routed = 
logging = 

[Rule10]
ufw_rule = 67/udp ALLOW OUT Anywhere (out)
description = DHCP
command = /usr/sbin/ufw allow out proto udp from any to any port 67
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 67/udp
iface = 
routed = 
logging = 

[Rule11]
ufw_rule = 5353/udp ALLOW OUT Anywhere (out)
description = Avahi
command = /usr/sbin/ufw allow out proto udp from any to any port 5353
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 5353/udp
iface = 
routed = 
logging = 

[Rule12]
ufw_rule = 5298 ALLOW OUT Anywhere (out)
description = Avahi
command = /usr/sbin/ufw allow out from any to any port 5298
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 5298
iface = 
routed = 
logging = 

[Rule13]
ufw_rule = 53 ALLOW OUT Anywhere (out)
description = DNS
command = /usr/sbin/ufw allow out from any to any port 53
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 53
iface = 
routed = 
logging = 

[Rule14]
ufw_rule = 9050/tcp ALLOW OUT Anywhere (out)
description = tor
command = /usr/sbin/ufw allow out proto tcp from any to any port 9050
policy = allow
direction = out
protocol = tcp
from_ip = 
from_port = 
to_ip = 
to_port = 9050
iface = 
routed = 
logging = 

[Rule15]
ufw_rule = 123/udp (v6) ALLOW OUT Anywhere (v6) (out)
description = Protocole d'heure réseau (NTP)
command = /usr/sbin/ufw allow out proto udp from any to any port 123
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 123/udp
iface = 
routed = 
logging = 

[Rule16]
ufw_rule = 80/tcp (v6) ALLOW OUT Anywhere (v6) (out)
description = HTTP
command = /usr/sbin/ufw allow out proto tcp from any to any port 80
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 80/tcp
iface = 
routed = 
logging = 

[Rule17]
ufw_rule = 443/tcp (v6) ALLOW OUT Anywhere (v6) (out)
description = HTTPS
command = /usr/sbin/ufw allow out proto tcp from any to any port 443
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 443/tcp
iface = 
routed = 
logging = 

[Rule18]
ufw_rule = 6667/tcp (v6) ALLOW OUT Anywhere (v6) (out)
description = IRC
command = /usr/sbin/ufw allow out proto tcp from any to any port 6667
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 6667/tcp
iface = 
routed = 
logging = 

[Rule19]
ufw_rule = 6697/tcp (v6) ALLOW OUT Anywhere (v6) (out)
description = IRC SSL
command = /usr/sbin/ufw allow out proto tcp from any to any port 6697
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 6697/tcp
iface = 
routed = 
logging = 

[Rule20]
ufw_rule = 143/tcp (v6) ALLOW OUT Anywhere (v6) (out)
description = IMAP
command = /usr/sbin/ufw allow out proto tcp from any to any port 143
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 143/tcp
iface = 
routed = 
logging = 

[Rule21]
ufw_rule = 6881:6891/tcp (v6) ALLOW OUT Anywhere (v6) (out)
description = Deluge Torrent
command = /usr/sbin/ufw allow out proto tcp from any to any port 6881:6891
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 6881:6891/tcp
iface = 
routed = 
logging = 

[Rule22]
ufw_rule = 6881:6891/udp (v6) ALLOW OUT Anywhere (v6) (out)
description = Deluge Torrent
command = /usr/sbin/ufw allow out proto udp from any to any port 6881:6891
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 6881:6891/udp
iface = 
routed = 
logging = 

[Rule23]
ufw_rule = 110/tcp (v6) ALLOW OUT Anywhere (v6) (out)
description = POP3
command = /usr/sbin/ufw allow out proto tcp from any to any port 110
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 110/tcp
iface = 
routed = 
logging = 

[Rule24]
ufw_rule = 25/tcp (v6) ALLOW OUT Anywhere (v6) (out)
description = SMTP
command = /usr/sbin/ufw allow out proto tcp from any to any port 25
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 25/tcp
iface = 
routed = 
logging = 

[Rule25]
ufw_rule = 67/udp (v6) ALLOW OUT Anywhere (v6) (out)
description = DHCP
command = /usr/sbin/ufw allow out proto udp from any to any port 67
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 67/udp
iface = 
routed = 
logging = 

[Rule26]
ufw_rule = 5353/udp (v6) ALLOW OUT Anywhere (v6) (out)
description = Avahi
command = /usr/sbin/ufw allow out proto udp from any to any port 5353
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 5353/udp
iface = 
routed = 
logging = 

[Rule27]
ufw_rule = 5298 (v6) ALLOW OUT Anywhere (v6) (out)
description = Avahi
command = /usr/sbin/ufw allow out from any to any port 5298
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 5298
iface = 
routed = 
logging = 

[Rule28]
ufw_rule = 53 (v6) ALLOW OUT Anywhere (v6) (out)
description = DNS
command = /usr/sbin/ufw allow out from any to any port 53
policy = allow
direction = out
protocol = 
from_ip = 
from_port = 
to_ip = 
to_port = 53
iface = 
routed = 
logging = 

[Rule29]
ufw_rule = 9050/tcp (v6) ALLOW OUT Anywhere (v6) (out)
description = tor
command = /usr/sbin/ufw allow out proto tcp from any to any port 9050
policy = allow
direction = out
protocol = tcp
from_ip = 
from_port = 
to_ip = 
to_port = 9050
iface = 
routed = 
logging = 

