## Gufw

Pare-feu

TCP
- >80/443 http/https 
- >143/110/25 IMAP/POP3/SMTP
- >6881:6891 deluged (UDP)
- >6667 IRC
- >111,2049,32765:32768 NFS
- TOR

UDP
- >123 NTPd
- >68 dhclient
- <5353,32768 avahi

LOCAL ONLY
- >631 CUPS
