echo "* Hexchat's config"
f="$HOME/.config/hexchat/hexchat.conf"
if [ -f "$f" ]; then
#Username
	sed -ri "s|(irc_nick1 = ).*|\1$USER|" "$f"
	sed -ri "s|(irc_nick2 = ).*|\1$USER|" "$f"
	sed -ri "s|(irc_nick3 = ).*|\1$USER|" "$f"
	sed -ri "s|(irc_user_name = ).*|\1$USER|" "$f"
#Dldir
	sed -ri "s|(dcc_dir = ).*|\1$XDG_DOWNLOAD_DIR|" "$f"
fi