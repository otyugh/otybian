## Firefox

*Navigateur web*

- Modules préinstallés
	- Ublock (bloqueur de pub)
	- HttpsEverywhere (force une connexion sécurisé quand c'est possible)
- Marques pages d'exemples
- Personnalisation de l'interface (disposition des icones)
- Moteur des recherche sympas par défaut
- Désactivation de la recherche dans l'urlbar
	- keyword.enabled;false
	- browser.fixup.alternate.enabled;false)
- Désactivation de comportements qui confusent les débutants
	- browser.tabs.warnOnClose = false #demander confirmation quand y a des onglets
	- browser.backspace_action = 1 #pour éviter que la touche d'effacement fassee un précédente
	- browser.sessionstore.resume_from_crash = false #ma grand mère a 20 fenêtre qui s'ouvrent au démarrage a cause de ça


### Détail
Ça m'a pris une blinde de temps mais j'ai fini par trouver "l'essentiel" pour configurer ff :

- xulstore.json : personnalisation de l'interface
- places.sqlite : les marque-pages
- prefs.js : préférences via about:config
- search.json.mozlz4 : moteur de recherche
- addonStartup.json.lz4 : gère la langue (retourne en anglais si supprimé)



- chrome/userChrome.css : thèmes css custom


- extensions/ + extensions.json + extension-settings.json : plugins