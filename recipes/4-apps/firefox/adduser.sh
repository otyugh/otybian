echo "* Firefox's corrected paths"
oldconf=$(find "$HOME/.mozilla/firefox/" -maxdepth 1 -name "*.default-esr" -print -quit)
ffprofile1="$(uuidgen -r | sed 's|-||g').default-esr"
#ffprofile2="$(uuidgen -r | sed 's|-||g')"
conf="$HOME/.mozilla/firefox/$ffprofile1"

if [ -d "$oldconf" ]; then
#randomize mozilla's profile
	mv "$oldconf" "$conf"
	f="$HOME/.mozilla/firefox/profiles.ini"
	ff="$HOME/.mozilla/firefox/installs.ini"
	sed -ri "s|^(Path=).*|\1$ffprofile1|g" "$f"
	sed -ri "s|^(Default=).*|\1$ffprofile1|g" "$f"
#	sed -ri "s|^(\[Install).*\]$|\1$ffprofile2]|" "$f"

#	sed -ri "s|^\[.*\]$|[$ffprofile2]|" "$ff"
	sed -ri "s|^(Default=).*|\1$ffprofile1|" "$ff"

	if [ -d "$conf" ]; then
	#firefox fucking format é_è
		f="${conf}/addonStartup.json.lz4"
		if [ -f "$f" ]; then
			tmp=$(mktemp)
			'mozlz4a.py' -d "$f" "$tmp"
			sed -i "s|/home/[^/]*|$HOME|g" "$tmp"
			'mozlz4a.py' "$tmp" "$f" 
		fi
	#Dldir && welcomepage
		f="${conf}/prefs.js"
		if [ -f "$f" ]
		then
			sed -ri "s|(user_pref\(\"browser.download.dir\", \").*(\"\);)|\1$XDG_DOWNLOAD_DIR\2|" "$f"
			#sed -ri "s|(user_pref\(\"browser.startup.homepage\", \").*(\"\);)|\1about:home\2|" "$f"
		fi

	#extension
		f="${conf}/extensions.json"
		#[ -f "$f" ] && sed -ri "s|(,\"path\":\").*\.default-esr(.*net\.xpi\")|\1$conf\2|" "$f"
		[ -f "$f" ] && sed -i "s|/home/[^/]+/|$HOME|" "$f"
	fi
fi