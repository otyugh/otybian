echo "* Grsync's config"
f="$HOME/.grsync/grsync.ini"
if [ -f "$f" ]; then
	sed -ri "s|^(text_source=).*|\1$HOME|" "$f"
	sed -ri "s|^(text_dest=).*|\1/media/$USER/SAUVEGARDE|" "$f"
fi