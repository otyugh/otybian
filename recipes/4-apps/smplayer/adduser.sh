echo "* Smplayer's config"
f="$HOME/.config/smplayer/smplayer.ini"
if [ -f "$f" ]
then
	sed -ri "s|^(screenshot_folder=).*|\1$XDG_DESKTOP_DIR|" "$f"
	sed -ri "s|^(latest_dir=).*|\1$XDG_VIDEOS_DIR|" "$f"
fi

