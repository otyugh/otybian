## Applications non-préconfigurées

*Ajoutées sans aucune changement*

- cheese
- handbrake
- gparted
- xarchiver unrar-free
- gpicview
- fbreader
- gdebi
- keepassx
- galculator
- gnome-system-tools
- system-config-printer cups foomatic-db
- lxtask
- blueman
