echo "* Thunderbird conf"
oldconf=$(find "$HOME/.thunderbird/" -maxdepth 1 -name "*.default" -print -quit)
conf="$HOME/.thunderbird/$(uuidgen -r | sed 's|-||g').default"
mv "$oldconf" "$conf"
f="$HOME/.thunderbird/profiles.ini"
sed -ri "s|^(Path=).*|\1$(basename $conf)|" "$f"
