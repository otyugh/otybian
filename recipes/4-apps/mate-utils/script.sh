#Search tool
conf="$chrootP/etc/skel/.config/Thunar/uca.xml"
sed -i '$ d' "$conf"
echo "<action>
        <icon>catfish</icon>
        <name>Rechercher ici</name>
        <unique-id>1560302039301448-2</unique-id>
        <command>mate-search-tool --path=%f</command>
        <description>Recherche de fichier par nom</description>
        <patterns>*</patterns>
        <directories/>
</action>
</actions>" >> "$conf"

#Screenshot
sed -ri 's|(<property name="Print" type="string" value=")[^"]*("/>)|\1mate-screenshot -i\2|' "$chrootP/etc/skel/.config/xfce4/xfconf/xfce-perchannel-xml/xfce4-keyboard-shortcuts.xml"

#Disk analyze space
mkdir -p "$chrootP/etc/dconf/db/site.d/"
echo "	[org/mate/disk-usage-analyzer/ui]
	active-chart='treemap'
" >> "$chrootP/etc/dconf/db/site.d/disk-usage-analyzer"
