echo "* Clementine's config"
f="$HOME/.config/Clementine/Clementine.conf"
if [ -f "$f" ]
then
	sed -ri "s|^(file_path=).*|\1$XDG_MUSIC_DIR|" "$f"
	sed -ri "s|^(download_dir=).*|\1$XDG_MUSIC_DIR|" "$f"
	sed -ri "s|^(last_path=).*|\1$XDG_MUSIC_DIR|" "$f"
fi