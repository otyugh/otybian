conf="$chrootP/etc/skel/.config/Thunar/uca.xml"
sed -i '$ d' "$conf"
echo "<action>
        <icon>catfish</icon>
        <name>Rechercher ici</name>
        <unique-id>1560302039301448-1</unique-id>
        <command>gnome-search-tool --path=%f</command>
        <description>Recherche ciblée</description>
        <patterns>*</patterns>
        <directories/>
</action>
</actions>" >> "$conf"