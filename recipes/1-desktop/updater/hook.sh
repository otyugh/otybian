#echo "* Enabling security auto-upgrade with unattended-upgrade + auto update everyday after 20sec max uptime"
#echo 'APT::Periodic::Enable "1";
#APT::Periodic::Update-Package-Lists "1";
#APT::Periodic::Download-Upgradeable-Packages "0";
#APT::Periodic::Unattended-Upgrade "0";
#APT::Periodic::AutocleanInterval "7";
#APT::Periodic::MaxAge "30";
#APT::Periodic::MinAge "2";
#APT::Periodic::MaxSize "500";
#APT::Periodic::RandomSleep "3600";
#' > "/etc/apt/apt.conf.d/10periodic"

echo "* Disabling auto upgrade (because there is  upgrade button) + let the default (update everyday)"
sed -ri 's|^(APT::Periodic::Unattended-Upgrade ")1(";)|\10\2|' "/etc/apt/apt.conf.d/20auto-upgrades"

#echo "* Toute MàJ officielle automatique"
#sed -ri 's|^//(      "origin=Debian,codename=${distro_codename}-updates";)|\1|' "/etc/apt/apt.conf.d/50unattended-upgrades"
#sed -ri 's|^//(      "origin=Debian,codename=${distro_codename}-proposed-updates";)|\1|' "/etc/apt/apt.conf.d/50unattended-upgrades"
#apt-get purge package-update-indicator
#apt-get install unattended-upgrades 

d='/etc/systemd/system/apt-daily.timer.d'
mkdir -p "$d"
echo "[Timer]
OnBootSec=3min
OnUnitActiveSec=1d
AccuracySec=1min
RandomizedDelaySec=1min
" > "$d/everyday.conf"

mkdir -p "/etc/skel/.config/package-update-indicator"
echo "[General]
update-command='gpk-update-viewer'
use-mobile-connection=false
" > "/etc/skel/.config/package-update-indicator/package-update-indicator.conf"
