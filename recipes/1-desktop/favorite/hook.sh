f="/etc/skel/.config/xfce4/panel/whiskermenu-1.rc"
sed -ri 's|(favorites=).*|\1firefox-esr.desktop,falkon.desktop,thunderbird.desktop,claws-mail.desktop,Thunar.desktop|' "$f"
#org.keepassxc.KeePassXC.desktop,xfce4-terminal.desktop

f="/etc/skel/.config/mimeapps.list"
img="mirage.desktop;gimp.desktop;"
pdf="atril.desktop;evince.desktop;gimp.desktop;"
txt="mousepad.desktop;"

sed -i "s|IMG|$img|" "$f"
sed -i "s|TXT|$txt|" "$f"
sed -i "s|PDF|$pdf|" "$f"