echo "\
[apps/light-locker]
late-locking=false
lock-after-screensaver=uint32 0
lock-on-suspend=false
idle-hint=false
lock-on-lid=false
" > '/etc/dconf/db/site.d/lightlocker'