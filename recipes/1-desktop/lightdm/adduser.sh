if [ $uid -eq 1000 ]
then

echo "* [Once] Auto-login in lightdm"
sed -ri "s|^#(autologin-user=)|\1$USER|;s|^#(autologin-user-timeout=0)|\1|" '/etc/lightdm/lightdm.conf'

fi