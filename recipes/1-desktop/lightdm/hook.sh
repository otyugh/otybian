echo "* Auto-numlock in lightdm"
sed -ri "s|^#(greeter-setup-script=)|\1/usr/bin/numlockx on|" '/etc/lightdm/lightdm.conf'

echo "* Show seats"
sed -ri "s|^#(greeter-hide-users=)|\1false|" '/etc/lightdm/lightdm.conf'
