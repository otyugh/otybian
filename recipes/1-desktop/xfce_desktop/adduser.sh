echo "* Thunar's config"
f="$HOME/.config/xfce4/xfconf/xfce-perchannel-xml/thunar.xml"
[ -f "$f" ] && sed -ri "s|(file://)/home/[^/\"]*|\1$HOME|" "$f"

echo "* Bookmark's config"
f="$HOME/.config/gtk-3.0/bookmarks"
mkdir -p "$(dirname $f)"
echo "file://$XDG_DOCUMENTS_DIR
file://$XDG_PICTURES_DIR
file://$XDG_MUSIC_DIR
file://$XDG_VIDEOS_DIR
" > "$f"
#file://$XDG_DOWNLOAD_DIR"
chown -R $uid:$uid "$(dirname $f)"

echo "* Xfce4-panel's thunar icon in panel"
find "$HOME/.config/xfce4/panel" -type f -name "*.desktop" -exec sed -ri "s|^(Exec=thunar ).*|\1$XDG_DOCUMENTS_DIR|" {} +

echo "* Xfce's config"
f="$HOME/.config/menus/xfce-applications.menu"
[ -f "$f" ] && sed -ri "s|^(.*<DirectoryDir>).*(/.local/share/desktop-directories</DirectoryDir>)|\1$HOME\2|" "$f"

#echo "* Xfce's thunar launcher"
#sed -ri "s|^(Exec=thunar ).*|\1$XDG_DOCUMENTS_DIR|" "$HOME/.config/xfce4/panel/launcher-7/documents.desktop"
#sed -ri "s|^(Exec=thunar ).*|\1$XDG_PICTURES_DIR|" "$HOME/.config/xfce4/panel/launcher-7/images.desktop"
#sed -ri "s|^(Exec=thunar ).*|\1$XDG_VIDEOS_DIR|" "$HOME/.config/xfce4/panel/launcher-7/videos.desktop"
#sed -ri "s|^(Exec=thunar ).*|\1$XDG_MUSIC_DIR|" "$HOME/.config/xfce4/panel/launcher-7/musique.desktop"


sed -ri 's|^(NoDisplay=)true|\1false|' '/etc/xdg/autostart/light-locker.desktop'
echo 'Hidden=true' >> '/etc/xdg/autostart/light-locker.desktop'