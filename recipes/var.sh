FLAVOUR="buster"
ARCHITECTURE="64"

ISONAME="Otybian"
PUBLISHER="Otyugh"
ISOPUBLISHER="Otybian; https://framagit.org/otyugh/otybian; otyugh@arzinfo.pw"

WALLPAPER="/usr/share/backgrounds/space2.jpg"
THEME="Greybird"
THEME_ICON="elementary-xfce-dark"
THEME_XFWM="Default"
